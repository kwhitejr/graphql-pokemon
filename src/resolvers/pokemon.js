const rp = require("request-promise");

const BASE_URL = "https://pokeapi.co/api/v2/pokemon";

const getPokemonById = id => {
  console.log(`attempting to query pokemon with id ${id}`)

  const rpOptions = {
    uri: `${BASE_URL}/${id}/`,
    headers: {
      'User-Agent': 'Request-Promise'
    },
    json: true // Automatically parses the JSON string in the response
  };
  return rp(rpOptions).catch(err => console.error(err));
}

module.exports = {
  getPokemonById
}