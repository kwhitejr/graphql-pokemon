const { GraphQLInt, GraphQLObjectType } = require('graphql');
const { PokemonType } = require('../types/pokemon');
const { getPokemonById } = require("../resolvers/pokemon");

const pokemonQuery = new GraphQLObjectType({
  name: "Query",
  fields: {
    pokemon: {
      type: PokemonType,
      args: {
        id: { type: GraphQLInt }
      },
      resolve: (_, { id }) => {
        return getPokemonById(id);
      }
    }
  }
});

module.exports = {
  pokemonQuery
};
