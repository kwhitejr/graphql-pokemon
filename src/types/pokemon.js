const {
  GraphQLList,
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean
} = require("graphql");

const AbilityType = new GraphQLObjectType({
  name: "abilityMeta",
  fields: {
    is_hidden: { type: GraphQLBoolean },
    slot: { type: GraphQLInt },
    ability: {
      type: new GraphQLObjectType({
        name: "ability",
        fields: {
          name: { type: GraphQLString },
          url: { type: GraphQLString }
        }
      })
    }
  }
});

const PokemonType = new GraphQLObjectType({
  name: "pokemon",
  fields: {
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    base_experience: { type: GraphQLInt },
    height: { type: GraphQLInt },
    is_default: { type: GraphQLBoolean },
    order: { type: GraphQLInt },
    weight: { type: GraphQLInt },
    abilities: { type: new GraphQLList(AbilityType) }
  }
});

module.exports = {
  PokemonType,
  AbilityType
};
