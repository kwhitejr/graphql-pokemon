const express = require("express");
const graphqlHTTP = require("express-graphql");

const { schema } = require("./schema");

const app = express();
const PORT = 3000;

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    graphiql: true
  })
);

app.get("/", (req, res) => {
  res.send("Try http://localhost:3000/graphiql");
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
