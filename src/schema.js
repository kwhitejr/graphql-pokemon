const {
  GraphQLObjectType,
  GraphQLSchema,
} = require("graphql");

const { pokemonQuery } = require("./queries/pokemon")

const schema = new GraphQLSchema({
  query: pokemonQuery
});

module.exports = {
  schema
};